__author__ = 'Arik Senderovich'

import xgboost as xgb
import numpy as np
import csv
import numpy
from pytz import *
from collections import namedtuple
from decimal import Decimal

import gc
from scipy.stats import norm
import math
from collections import OrderedDict
from operator import itemgetter
import xml.etree.ElementTree as ET
from scipy import stats
from sklearn import datasets as skds
import pandas as pio
from sklearn.preprocessing import LabelEncoder
from operator import attrgetter
from collections import namedtuple
from sklearn import svm
from sklearn.cross_validation import train_test_split
from sklearn.preprocessing import OneHotEncoder
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Lasso, LassoCV, LassoLarsCV
from sklearn.metrics import mean_squared_error, mean_absolute_error
from math import sqrt
from sklearn.feature_selection import RFE, RFECV
from sklearn.metrics import roc_auc_score
from sklearn.svm import SVR
from sklearn import ensemble
from sklearn.grid_search import GridSearchCV
from datetime import datetime as d_time
from sklearn.preprocessing import Imputer






def history_encoding_new(df):
    # encode history: (without time)
    hist_len = get_history_len(df)

    for k in range(0, hist_len):
        df['event_' + str(k)] = -1
    for i in range(0, len(df)):
        if str(df.at[i, 'history']) != "nan":
            parsed_hist = str(df.at[i, 'history']).split("_")
            for k in range(0, len(parsed_hist)):
                df.at[i, 'event_' + str(k)] = int(parsed_hist[k])
    return df, hist_len


def history_encoding_memory(df):
    # encode history: (without time)
    hist_len = get_history_len(df)
    # Upper bound on history (sliding window) of w = memorylen
    memorylen = 3

    hist_len = min(memorylen, hist_len)
    for k in range(0, hist_len):
        df['event_' + str(k)] = -1

    for i in range(0, len(df)):
        if str(df.at[i, 'history']) != "nan":
            parsed_hist = str(df.at[i, 'history']).split("_")
            for k in range(0, len(parsed_hist)):
                if k <= len(parsed_hist) - memorylen - 1:
                    break
                else:
                    df.at[i, 'event_' + str(k)] = int(parsed_hist[len(parsed_hist) - k - 1])
    return df, hist_len


def prep_data(df, link_list, state_list, query_name):


	#!Here: insert \alpha features available in the log as categorical variables
    cols = ['state','staff', 'gender', 'arrival','history']
    #!Here: History encoding, choose either full history or window of size w.
    #df,hist_len = history_encoding_memory(df)
    df, hist_len = history_encoding_new(df)
	
    for h in range(0,hist_len):
      cols.append('event_'+str(h))

    for c in cols:
        df[c] = df[c].astype('category')

    df_categorical = df[cols]

    dummies = pio.get_dummies(df_categorical)
    #Here: non-categorical variables and target query name
	cols = ['age','elapsed_time', query_name]

    #Snapshot congestion graph:
    for l in link_list:
       cols.append('snapshot'+'_'+str(l[0])+ '_' + str(l[1]))

	#MSR congestion graph:
    #for s in state_list:
    #   cols.append('queue'+'_'+str(s))
    #   cols.append('ttiq' + '_' + str(s))
    #   cols.append('o_rate' + '_' + str(s))


    df_numerical = df[cols]
    df_numerical = pio.concat([df_numerical, dummies], axis=1)
    train_df, test_df = train_test_split(df_numerical, test_size=0.2, random_state=3)
  
    train_list = train_df.index.values.tolist()
    test_list = test_df.index.values.tolist()
    train_df = train_df.reset_index(drop=True)
    test_df = test_df.reset_index(drop=True)
    return train_df, test_df, train_list, test_list


def pre_proc(df, link_list):
    cols = []
    for l in link_list:
        cols.append('snapshot' + '_' + str(l[0]) + '_' + str(l[1]))
    num_arr = df[cols].values
	#Here: we impute missing snapshot values
    imp = Imputer(missing_values='NaN', strategy='mean', axis=0)
    imp.fit(num_arr)
    imputed = imp.transform(num_arr)
    for i in range(0, len(df)):
        for j in range(0, imputed.shape[1]):
            df.at[i, 'snapshot' + '_' + str(link_list[j][0]) + '_' + str(link_list[j][1])] = imputed[i][j]
    return


def Lasso_Regression(train_df, test_df):
        alpha_lasso = [1e-15, 1e-10, 1e-8, 1e-5, 1e-4, 1e-3, 1e-2, 1, 5, 10]
        LR = LassoCV(fit_intercept=True, normalize=True, n_jobs=-1, alphas=alpha_lasso, max_iter=100000, tol=0.001)
        X_train = train_df.ix[:, train_df.columns != 'remaining_time']
        X_test = test_df.ix[:, test_df.columns != 'remaining_time']
        # feat_list = ['elapsed_time','age','simple_snapshot','state','staff','gender','arrival']
        y = train_df['remaining_time']
        LR.fit(X_train, y)
        y_test = test_df['remaining_time']
        
        print LR.alpha_
        y_pred = LR.predict(X_test)
        rms = sqrt(mean_squared_error(y_test, y_pred))
        mae = mean_absolute_error(y_test, y_pred)
        print 'Lasso RMSE = ' + str(round(rms / 60, 2))
        print 'Lasso MAE = ' + str(round(mae / 60, 2))


def random_Forest_regression(train_df, test_df, n_est, sample_leaf, query_name):
    rf = RandomForestRegressor(n_estimators=n_est, n_jobs=8, verbose=1)  # ,
    # oob_score = True)
    X_train = train_df.ix[:, train_df.columns != query_name]
    X_test = test_df.ix[:, test_df.columns != query_name]
    # feat_list = ['elapsed_time','age','simple_snapshot','state','staff','gender','arrival']
    y = train_df[query_name]
    rf.fit(X_train, y)
    y_test = test_df[query_name]
    # prediction, bias, contributions = ti.predict(rf, X_test)
    # assert (numpy.allclose(prediction, bias + np.sum(contributions, axis=1)))
    # assert (numpy.allclose(rf.predict(X_test), bias + np.sum(contributions, axis=1)))
    print rf
    y_pred = rf.predict(X_test)
    y_pred[y_pred < 0] = 0
    rms = sqrt(mean_squared_error(y_test, y_pred))
    mae = mean_absolute_error(y_test, y_pred)
    print 'Random Forest RMSE = ' + str(round(rms / 60, 2))
    print 'Random Forest MAE = ' + str(round(mae / 60, 2))
    return y_pred





def XG_Boosting_Regression(train_df, test_df, n_est, md, query_name):
    X_train = train_df.ix[:, train_df.columns != query_name]
    X_test = test_df.ix[:, test_df.columns != query_name]

    # feat_list = ['elapsed_time','age','simple_snapshot','state','staff','gender','arrival']
    y = train_df[query_name]
    # params = {'n_estimators': 500, 'max_depth': 4, 'min_samples_split': 2,
    #   'learning_rate': 0.01, 'loss': 'ls'}
    clf = xgb.XGBRegressor(n_estimators=n_est, max_depth=md)  # , verbose = 1)
    # clf = GridSearchCV(xgb_model,
    #                {'max_depth': [6,8,10],
    #               'n_estimators': [200,250,300]}, verbose=1)

    # clf.fit(X_train, y)
    clf.fit(X_train, y)

    #xgb.plot_importance(clf)
    y_test = test_df[query_name]

    y_pred = clf.predict(X_test)
    y_pred[y_pred < 0] = 0
    rms = sqrt(mean_squared_error(y_test, y_pred))
    mae = mean_absolute_error(y_test, y_pred)
    print 'XGB RMSE = ' + str(round(rms / 60, 2))
    print 'XGB MAE = ' + str(round(mae / 60, 2))

    # plot_deviance(clf,y_test,X_test,n_est)
    return y_pred


def XG_Boosting_Regression_CV(train_df, test_df, n_est, md, query_name):
    # cross validating XGBoost
    train_df_new, eval_df = train_test_split(train_df, test_size=0.25, random_state=3)
    X_train = train_df_new.ix[:, train_df.columns != query_name]
    X_eval = eval_df.ix[:, train_df.columns != query_name]
    X_test = test_df.ix[:, test_df.columns != query_name]
    # feat_list = ['elapsed_time','age','simple_snapshot','state','staff','gender','arrival']
    y = train_df_new[query_name]
    y_eval = eval_df[query_name]
    # params = {'n_estimators': 500, 'max_depth': 4, 'min_samples_split': 2,
    #   'learning_rate': 0.01, 'loss': 'ls'}
    clf = xgb.XGBRegressor(max_depth=md, n_estimators=n_est)  # , verbose = 1)

    eval_list = [(X_eval, y_eval)]
    clf.fit(X_train, y, eval_set=eval_list, verbose=True, early_stopping_rounds=100)
    #xgb.plot_importance(clf)
    y_test = test_df[query_name]
    y_pred = clf.predict(X_test)
    y_pred[y_pred < 0] = 0
    rms = sqrt(mean_squared_error(y_test, y_pred))
    mae = mean_absolute_error(y_test, y_pred)
    print 'XGB RMSE = ' + str(round(rms / 60, 2))
    print 'XGB MAE = ' + str(round(mae / 60, 2))

    # plot_deviance(clf,y_test,X_test,n_est)
    return y_pred


def linear_regression(train_df, test_df, query_name):
    lm = LinearRegression(fit_intercept=True)
    X_train = train_df.ix[:, train_df.columns != query_name]
    X_test = test_df.ix[:, test_df.columns != query_name]
    # feat_list = ['elapsed_time','age','simple_snapshot','state','staff','gender','arrival']
    y = train_df[query_name]
    lm.fit(X_train, y)
    print lm
    y_pred = lm.predict(X_test)
    y_pred[y_pred < 0] = 0
    y_test = test_df[query_name]

    rms = sqrt(mean_squared_error(y_test, y_pred))
    mae = mean_absolute_error(y_test, y_pred)
    print 'Linear Regression RMSE = ' + str(round(rms / 60, 2))
    print 'Linear Regression MAE = ' + str(round(mae / 60, 2))
    return y_pred


def ML_methods(df, link_list, state_list, query_name):
    # df = df.reset_index(drop=True)
    train_df, test_df, train_list, test_list = prep_data(df, link_list, state_list, query_name)
    print "Training shape is: " + str(train_df.shape)
    # print "Linearized snapshot"
    # y_train, y_test = linear_regression_snap(train_df, test_df, link_list)
    # train_df, test_df = case_only(df, link_list, train_list, test_list, y_train, y_test)
    print "Fitting Random Forest"
    trees = [10,20,50]
    rf_results = []
    for t in trees:
        print "Number of trees: " + str(t)
        rf_results.append(random_Forest_regression(train_df, test_df, t, 2, query_name))
    print "Fitting XGBOOST"
    trees_xg = [6000]
    xg_results = []
    
    for t in trees_xg:
        print "Number of trees: " + str(t)
        #xg_results.append(XG_Boosting_Regression_CV(train_df, test_df, t, 10, query_name))
        xg_results.append(XG_Boosting_Regression(train_df, test_df, t, 10, query_name))
    print "Fitting Lasso CV"
    Lasso_Regression(train_df, test_df)
    print "Fitting Linear Regression"
    y_linear = linear_regression(train_df, test_df, query_name)

    return


def cut_outcome(df):
    
    outcome_flag = False
    forbidden = False
    count_forbidden = 0
    cur_id = df.at[0, 'id']
    x = []
    for i, row in df.iterrows():
        # print str(i) + ' out of ' + str(num_rows)
        if cur_id == df.at[i, 'id']:
            # cur_id = df.at[i,'id']
            if forbidden == False:
                # cur_id = df.at[i,'id']
                if outcome_flag == True:
                    # df.drop(i, inplace=True)
                    continue
                try:
                    cur_state = df.at[i, 'state']
                    if cur_state == 7:
                        outcome_flag = True

                    x.append(df.loc[[i]].values.tolist()[0])
                    # new_df.loc[len(new_df)] = df.loc[[i]].values
                except KeyError:
                    continue
            else:
                continue


        else:
            cur_id = df.at[i, 'id']
            outcome_flag = False
            if df.at[i, 'state'] == 1:
                x.append(df.loc[[i]].values.tolist()[0])
                forbidden = False
            else:
                forbidden = True
                count_forbidden += 1
    # print count_forbidden
    new_df = pio.DataFrame(x,
                           columns=['id', 'timestamp', 'state', 'staff', 'gender', 'age', 'discharge', 'arrival',
                                    'date_time'])
    
    return new_df


def read_into_panda_from_csv(path):
    # format assumption:
    # id_medical,start_time,sub_event,staff,gender,age,discharge_status,arrival_type_group,start_time_date
    panda_log = pio.read_csv(filepath_or_buffer=path, header=0, index_col=0)
    # rename columns:
    panda_log.columns = ['id', 'patient_id', 'state', 'timestamp', 'date_time', 'diagnosis', 'link_flag']
    # panda_log['hr'] = 0
    # for j,dt in enumerate(panda_log['date_time'].values.tolist()):
    #    panda_log.at[j,'hr'] = dt.hour
    panda_log = panda_log.sort(['id', 'timestamp'], ascending=True)
    panda_log = panda_log.reset_index(drop=True)
    # panda_log= cut_outcome(panda_log)
    # panda_log = panda_log[panda_log.state!=4]
    # panda_log = panda_log.reset_index(drop=True)

    return panda_log


def add_next_state(df):
    df['next_state'] = ''
    df['next_time'] = 0
    df['next_dur'] = 0
    num_rows = len(df)
    for i in range(0, num_rows - 1):
        print str(i) + ' out of ' + str(num_rows)

        if df.at[i, 'id'] == df.at[i + 1, 'id']:
            df.at[i, 'next_state'] = df.at[i + 1, 'state']
            df.at[i, 'next_time'] = df.at[i + 1, 'timestamp']
            df.at[i, 'next_dur'] = df.at[i + 1, 'timestamp'] - df.at[i, 'timestamp']
        else:
            df.at[i, 'next_state'] = 99
            df.at[i, 'next_time'] = df.at[i, 'timestamp']
            df.at[i, 'next_dur'] = 0
    return df


def add_query_remaining(df):
    df['elapsed_time'] = 0
    df['total_time'] = 0
    df['remaining_time'] = 0
    df['history'] = ""
    ids = []
    total_Times = []
    num_rows = len(df)
    temp_elapsed = 0
    prefix = str(df.at[0, 'state'])

    for i in range(1, num_rows):
        # print i
        print str(i) + ' out of ' + str(num_rows)

        if df.at[i, 'id'] == df.at[i - 1, 'id']:
            temp_elapsed += df.at[i - 1, 'next_dur']
            df.at[i, 'elapsed_time'] = temp_elapsed
            prefix = prefix + '_' + str(df.at[i, 'state'])
            df.at[i, 'history'] = prefix
        else:
            ids.append(df.at[i - 1, 'id'])
            total_Times.append(temp_elapsed)
            temp_elapsed = 0
            prefix = str(df.at[i, 'state'])

    ids.append(df.at[num_rows - 1, 'id'])
    total_Times.append(df.at[num_rows - 1, 'elapsed_time'])
    # df.at[num_rows-1,'elapsed_time'] = temp_elapsed
    for i in range(0, num_rows):
        print str(i) + ' out of ' + str(num_rows)
        try:
            ind = ids.index(df.at[i, 'id'])
            total_ = total_Times[ind]
            df.at[i, 'total_time'] = total_
            df.at[i, 'remaining_time'] = total_ - df.at[i, 'elapsed_time']
            # print df.head(i)
        except ValueError:
            print 'err'
            return ValueError
    return


def is_empty(any_structure):
    if any_structure:
        print('Structure is not empty.')
        return False
    else:
        print('Structure is empty.')
        return True


def find_snapshot_in_np(snap_list, cur_time):
    #Restricting snapshot age
	snapshot_age = 52200
    snap_result = -1
    snap_index = -1
    for j in reversed(range(0, len(snap_list))):
        if snap_list[j][1] <= cur_time and (cur_time-snap_list[j][1])<=snapshot_age:
            snap_result = snap_list[j][1] - snap_list[j][0]
            snap_index = j
            break
    remove_indices = []
    if snap_index > -1:
        for j in range(0, snap_index):
            if snap_list[j][1] < cur_time:
                remove_indices.append(j)
        if len(remove_indices) > 0:
            for index in sorted(remove_indices, reverse=True):
                del snap_list[index]

        return snap_result
    else:
        return np.NaN


def add_snapshot(df, link_list):
   
    snapshot_queue = []
    tuple = []
    for l in link_list:
        col_name = 'snapshot' + '_' + str(l[0]) + '_' + str(l[1])
        df[col_name] = np.NAN
        snapshot_queue.append(tuple)
        tuple = []

    # prev_link = [-1,-1]
    num_rows = len(df)
    for i in range(0, num_rows):
        print (str(i) + ' snapshot calculation')
        # cur_state = r.state.values[0]
        cur_time = df.at[i, 'timestamp']
        for j, l in enumerate(link_list):
            col_name = 'snapshot' + '_' + str(l[0]) + '_' + str(l[1])
            ind = link_list.index(l)
            df.at[i, col_name] = find_snapshot_in_np(snapshot_queue[ind], cur_time)
        next_time = df.at[i, 'next_time']
        cur_link = [df.at[i, 'state'], df.at[i, 'next_state']]
        ind = link_list.index(cur_link)
        tuple = [cur_time, next_time]
        snapshot_queue[ind].append(tuple)
    return df


# def encode_sequence(df):
def write_pandas_to_csv(df, version, out):
    # df = df.reset_index(drop=True)
    if out == False:
        df.to_csv('Query_Remaining_Time' + str(version) + '.csv',sep=',')
    else:
        df.to_csv('Results/' + str(version) + '.csv', sep=',')


def create_initial_log(path):
    df = read_into_panda_from_csv(path)
    add_next_state(df)
    add_query_remaining(df)
    df = clean_outliers(df)
    version = "V_events_0"
    write_pandas_to_csv(df, version, False)


def long_term(df):
    arr = df['remaining_time'].values
    return np.mean(arr)


def compare_snapshot_long_term(df):
    train_df, test_df = train_test_split(df, test_size=0.2, random_state=3)
    train_df = train_df.reset_index(drop=True)
    test_df = test_df.reset_index(drop=True)

    lt_pred = float(long_term(train_df))
    lt_mse = 0
    snap_mse = 0
    num_rows = len(test_df)

    for i in range(0, num_rows):
        lt_mse += pow(lt_pred - test_df.at[i, 'remaining_time'], 2)
        if df.at[i, 'simple_snapshot'] != -1:
            snap_mse += pow(test_df.at[i, 'simple_snapshot'] - test_df.at[i, 'remaining_time'], 2)
        else:
            snap_mse += pow(lt_pred - test_df.at[i, 'remaining_time'], 2)

    lt_rmse = pow(lt_mse / num_rows, 0.5) / 60
    snap_rmse = pow(snap_mse / num_rows, 0.5) / 60
    return lt_rmse, snap_rmse


def clean_outliers(df):
    # looking for cases longer than 1 days = 345,600 seconds.
    case_length = 86400
    df = df[df.total_time < case_length]
    return df


def order_csv_time(path):
    df = pio.read_csv(filepath_or_buffer=path, header=0, index_col=0)  # , nrows= 20)
    df = df.sort(['timestamp'], ascending=True)
    df = df.reset_index(drop=True)
    version = "V_events_0_ordered"
    write_pandas_to_csv(df, version, False)


def read_from_query(path):
    df = pio.read_csv(filepath_or_buffer=path, header=0, index_col=0)  # ,nrows = 1000)
    # List = range(0,len(df))
    # df = df.ix[List]

    return df


def add_hour(df):
    df['hr'] = 0
    for i in range(0, len(df)):
        # if pio.isnull(df.at[i, 'next_state']):
        #    df.at[i, 'next_state'] = 7
        #    if df.at[i,'next_time'] == 0:
        #        df.at[i,'next_time']= df.at[i,'timestamp']
        try:
            df.at[i, 'hr'] = d_time.strptime(df.at[i, 'date_time'], "%m/%d/%Y %H:%M").hour
        except ValueError:
            df.at[i, 'hr'] = d_time.strptime(df.at[i, 'date_time'], "%m/%d/%Y").hour
        except TypeError:
            print 'Hr err'
    return


def get_links(df):
    link_list = []
    for i in range(0, len(df)):
        try:
            pair = [df.at[i, 'state'], int(df.at[i, 'next_state'])]
            try:
                ind = link_list.index(pair)
            except ValueError:
                link_list.append(pair)
        except KeyError:
            print 'Links err'

    return link_list


def get_states(df):
    state_list = []
    for i in range(0, len(df)):
        pair = df.at[i, 'state']
        try:
            ind = state_list.index(pair)
        except ValueError:
            state_list.append(pair)
    return state_list


def snapshot_level(path_query):
    df = read_from_query(path_query)
    add_hour(df)
    link_list = get_links(df)
    df = add_snapshot(df, link_list)
    version = "V_events_1"
    write_pandas_to_csv(df, version, False)
    pre_proc(df, link_list)
    version = "V_events_1_imputed"
    write_pandas_to_csv(df, version, False)
    return


def select_log(path_query):
    df = pio.read_csv(filepath_or_buffer=path_query, header=0, index_col=0)  # ,nrows = 1000)
    List = range(146522, 481782)
    # Selecting DFCI - up to
    df = df.ix[List]
    df = df.reset_index(drop=True)
    version = "V_events_0_selected"
    write_pandas_to_csv(df, version, False)

    return


def update_event_queue(event_queue, cur_time):
    remove_indices = []
    rem_ind = []

    # going over the different states and getting the rates
    for i, e in enumerate(event_queue):
        for j, q in enumerate(event_queue[i]):
            if q[1] <= cur_time:
                rem_ind.append(j)
        remove_indices.append(rem_ind)

        # print 'count remove: ' + str(count_remove)
        count_remove = 0
        if len(remove_indices[i]) > 0:
            for index in sorted(remove_indices[i], reverse=True):
                del event_queue[i][index]
        rem_ind = []
    return


def find_q_len_ttiq(event_queue, cur_time):
    q_len = len(event_queue)
    ttiq = 0

    for e in event_queue:
        ttiq += (cur_time - e[0])

    return q_len, ttiq


def add_queues(df, state_list):
    event_queue = []
    tuple = []
    recent_occur = []
    delta = []
    for s in state_list:
        col_name = 'queue' + '_' + str(s)
        df[col_name] = 0
        col_name = 'ttiq' + '_' + str(s)
        df[col_name] = 0
        col_name = 'o_rate' + '_' + str(s)
        df[col_name] = 0
        recent_occur.append(0)
        delta.append(0)
        event_queue.append(tuple)
        tuple = []

    num_rows = len(df)
    for i in range(0, num_rows):
        print (str(i) + ' queueing calculation')
        # cur_state = r.state.values[0]
        cur_time = df.at[i, 'timestamp']
        next_time = df.at[i, 'next_time']
        cur_state = df.at[i, 'state']
        ind = state_list.index(cur_state)
        tuple = [cur_time, next_time]
        event_queue[ind].append(tuple)
        update_event_queue(event_queue, cur_time)

        if recent_occur[ind] == 0:
            recent_occur[ind] = df.at[i, 'timestamp']
        else:
            delta[ind] = df.at[i, 'timestamp'] - recent_occur[ind]
            recent_occur[ind] = df.at[i, 'timestamp']

        for j, s in enumerate(state_list):
            col_name1 = 'queue' + '_' + str(s)
            col_name2 = 'ttiq' + '_' + str(s)
            col_name3 = 'o_rate' + '_' + str(s)

            ind = state_list.index(s)
            df.at[i, col_name1], df.at[i, col_name2] = find_q_len_ttiq(event_queue[ind], cur_time)
            df.at[i, col_name3] = delta[j]
    return df


def queue_level(path_query):
    df = read_from_query(path_query)
    # add_hour(df)
    state_list = get_states(df)
    df = add_queues(df, state_list)
    version = "V_events_3"
    write_pandas_to_csv(df, version, False)
    return


def encode_DFCI_dict(path):
    df = pio.read_csv(filepath_or_buffer=path, header=0, index_col=0)
    # rename columns:
    df.columns = ['id', 'patient_id', 'state', 'timestamp', 'date_time', 'diagnosis', 'link_flag']

    dict = []
    for i in range(0, len(df)):
        try:
            ind = dict.index(df.at[i, 'state'])
            df.at[i, 'state'] = ind + 1
        except ValueError:
            dict.append(df.at[i, 'state'])
            df.at[i, 'state'] = len(dict)
    # todo: print dictionary.
    dict_df = pio.DataFrame(data=dict, columns=['Event'])
    dict_df.to_csv('C:\Users\Arik\PycharmProjects\KDD_Features\DFCI\DFCI_Dictionary.csv', sep=',')

    return df


def main_dfci(ML, snapshot, qlength, Initial, Order,  Select):
    
	#Set the path to the original event log
	path_orig = 'original_event_log.csv'
    
	
	path_to_order = 'Query_Remaining_TimeV_events_0.csv'
    path_query = 'Query_Remaining_TimeV_events_0_ordered.csv'
    path_selected = 'Query_Remaining_TimeV_events_0_selected.csv'
    path_snapshot = 'Query_Remaining_TimeV_events_1_imputed.csv'
    path_ML = 'Query_Remaining_TimeV_events_3.csv'

    
    if Initial == True:
        create_initial_log(path_encoded)
    if Order == True:
        order_csv_time(path_to_order)
    if Select == True:
        select_log(path_query)
    if snapshot == True:
        snapshot_level(path_selected)

    if qlength == True:
        queue_level(path_snapshot)

    if ML == True:
        query_name = 'remaining_time'
        df = read_from_query(path_ML)
        link_list = get_links(df)
        state_list = get_states(df)
        ML_methods(df, link_list, state_list, query_name)

#Here: choose the following steps: Initial, Select, Order, snapshot, qlength, ML.
#Set them to True one by one and run. 
main_dfci(ML=True, snapshot=False,
          qlength=False, Initial=False, Order=False,  Select=False)

